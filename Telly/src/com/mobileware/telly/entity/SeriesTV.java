package com.mobileware.telly.entity;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

public class SeriesTV implements Parcelable{
	@SerializedName("num")
	private int num;
	@SerializedName("img")
	private String img;
	@SerializedName("lnk")
	private String lnk;
	@SerializedName("name")
	private String name;
	@SerializedName("air")
	private String air;
	
	public SeriesTV(Parcel in){
		this.num = 0;
		this.img = "";
		this.lnk = "";
		this.name = "";		
		this.air = "";		
		
		leerObjetoParcel(in);
	}
	
	public SeriesTV(){
		this.num = 0;
		this.img = "";
		this.lnk = "";
		this.name = "";		
		this.air = "";	
	}
	
	
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getLnk() {
		return lnk;
	}
	public void setLnk(String lnk) {
		this.lnk = lnk;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAir() {
		return air;
	}
	public void setAir(String air) {
		this.air = air;
	}
	
	@Override
	public int describeContents() {		
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(getNum());
		dest.writeString(getImg());
		dest.writeString(getLnk());
		dest.writeString(getName());
		dest.writeString(getAir());
	}
	
	
	private void leerObjetoParcel(Parcel in){
		setNum(in.readInt());
		setImg(in.readString());
		setLnk(in.readString());
		setName(in.readString());
		setAir(in.readString());
	}
	
	
	public static final Parcelable.Creator<SeriesTV> CREATOR
    = new Parcelable.Creator<SeriesTV>() {
        public SeriesTV createFromParcel(Parcel in) {
            return new SeriesTV(in);
        }
 
        public SeriesTV[] newArray(int size) {
            return new SeriesTV[size];
        }
    };
	
}

 