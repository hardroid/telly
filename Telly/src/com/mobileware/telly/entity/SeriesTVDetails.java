package com.mobileware.telly.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class SeriesTVDetails implements Parcelable{
	
	private String name;
	private String air;
	private String img;
	private String info;
	private Crew [] crews;
	private Listings [] listings;
	
	
	public SeriesTVDetails(Parcel in){
		this.name = "";
		this.air = "";
		this.img = "";
		this.info = "";
		this.crews = null;
		this.listings = null;	
		
		leerObjetoParcel(in);
	}
	
	public SeriesTVDetails(){
		this.name = "";
		this.air = "";
		this.img = "";
		this.info = "";
		this.crews = null;
		this.listings = null;		
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAir() {
		return air;
	}
	public void setAir(String air) {
		this.air = air;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public Crew[] getCrews() {
		return crews;
	}
	public void setCrews(Crew[] crews) {
		this.crews = crews;
	}
	public Listings[] getListings() {
		return listings;
	}
	public void setListings(Listings[] listings) {
		this.listings = listings;
	}
	
	
	@Override
	public int describeContents() {		
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(getName());
		dest.writeString(getAir());
		dest.writeString(getImg());
		dest.writeString(getInfo());
		dest.writeTypedArray(crews, flags);
		dest.writeTypedArray(listings, flags);
	}
	
	
	private void leerObjetoParcel(Parcel in){
		setName(in.readString());
		setAir(in.readString());
		setImg(in.readString());
		setInfo(in.readString());
		setCrews(in.createTypedArray(Crew.CREATOR));
		setListings(in.createTypedArray(Listings.CREATOR));
	}
	
	
	public static final Parcelable.Creator<SeriesTVDetails> CREATOR
    = new Parcelable.Creator<SeriesTVDetails>() {
        public SeriesTVDetails createFromParcel(Parcel in) {
            return new SeriesTVDetails(in);
        }
 
        public SeriesTVDetails[] newArray(int size) {
            return new SeriesTVDetails[size];
        }
    };
}
