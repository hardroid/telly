package com.mobileware.telly.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class ShowTimes implements Parcelable{
	
	private String time;
	private String ampm;
	private String info;
	private String air;
	
	public ShowTimes(Parcel in){
		this.time = "";
		this.ampm = "";		
		this.info = "";		
		this.air = "";		
		
		leerObjetoParcel(in);
	}
	
	public ShowTimes(){
		this.time = "";
		this.ampm = "";		
		this.info = "";		
		this.air = "";		
	}
	
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getAmpm() {
		return ampm;
	}
	public void setAmpm(String ampm) {
		this.ampm = ampm;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public String getAir() {
		return air;
	}
	public void setAir(String air) {
		this.air = air;
	}
	
	
	@Override
	public int describeContents() {		
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(getTime());
		dest.writeString(getAmpm());
		dest.writeString(getInfo());
		dest.writeString(getAir());
	}
	
	//Metodo que permite leer el objeto Parcel para pasarlo a las propiedades del objeto.
	private void leerObjetoParcel(Parcel in){		
		setTime(in.readString());		
		setAmpm(in.readString());
		setInfo(in.readString());
		setAir(in.readString());
	}
	
	//Este objeto es necesario para poder utilizar las caracteristicas de un Parcelable.
	public static final Parcelable.Creator<ShowTimes> CREATOR
    = new Parcelable.Creator<ShowTimes>() {
        public ShowTimes createFromParcel(Parcel in) {
            return new ShowTimes(in);
        }
 
        public ShowTimes[] newArray(int size) {
            return new ShowTimes[size];
        }
    };
}
