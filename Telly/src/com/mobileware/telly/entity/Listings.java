package com.mobileware.telly.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class Listings implements Parcelable{
	private String day;
	private ShowTimes[] showTimes;
	
	public Listings(Parcel in){
		this.day = "";
		this.showTimes = null;		
		
		leerObjetoParcel(in);
	}
	
	public Listings(){
		this.day = "";
		this.showTimes = null;		
	}
	
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public ShowTimes[] getShowTimes() {
		return showTimes;
	}
	public void setShowTimes(ShowTimes[] showTimes) {
		this.showTimes = showTimes;
	}	
	
	@Override
	public int describeContents() {		
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(getDay());
		dest.writeTypedArray(getShowTimes(), flags);
	}
	
	
	private void leerObjetoParcel(Parcel in){		
		setDay(in.readString());		
		setShowTimes(in.createTypedArray(ShowTimes.CREATOR));
	}
	
	
	public static final Parcelable.Creator<Listings> CREATOR
    = new Parcelable.Creator<Listings>() {
        public Listings createFromParcel(Parcel in) {
            return new Listings(in);
        }
 
        public Listings[] newArray(int size) {
            return new Listings[size];
        }
    };
}
	