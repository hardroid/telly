package com.mobileware.telly.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class Crew implements Parcelable {
	private String img;
	private String name;
	private String charecter;
	
	public Crew(Parcel in){
		this.img = "";
		this.name = "";		
		this.charecter = "";		
		
		leerObjetoParcel(in);
	}
	
	public Crew(){
		this.img = "";
		this.name = "";		
		this.charecter = "";	
	}
	
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCharecter() {
		return charecter;
	}
	public void setCharecter(String charecter) {
		this.charecter = charecter;
	}
	
	@Override
	public int describeContents() {		
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(getImg());
		dest.writeString(getName());
		dest.writeString(getCharecter());
	}
	
	
	private void leerObjetoParcel(Parcel in){		
		setImg(in.readString());		
		setName(in.readString());
		setCharecter(in.readString());
	}
	
	
	public static final Parcelable.Creator<Crew> CREATOR
    = new Parcelable.Creator<Crew>() {
        public Crew createFromParcel(Parcel in) {
            return new Crew(in);
        }
 
        public Crew[] newArray(int size) {
            return new Crew[size];
        }
    };
}
