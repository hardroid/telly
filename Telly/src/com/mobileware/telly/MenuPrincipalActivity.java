package com.mobileware.telly;

import java.util.ArrayList;
import java.util.List;

import com.mobileware.telly.adapter.TellyAdapter;
import com.mobileware.telly.db.TellyDB;
import com.mobileware.telly.entity.SeriesTV;
import com.mobileware.telly.util.Utils;
import com.mobileware.telly.ws.WSTelly;

import android.app.ActionBar;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.widget.ListView;
import android.widget.Toast;


public class MenuPrincipalActivity extends FragmentActivity {

	private ListView listSeriesTV;
	private List<SeriesTV> values;
	private TellyAdapter adapter;
	private final String KEY_STATE = "outState";
	
	private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);
        context = this;
        boolean isOnLine = Utils.isOnlinee(context);
        setActionBarTitle(isOnLine);
        listSeriesTV = (ListView) findViewById(R.id.listSeriesTV);
        if(savedInstanceState == null){
        	if(isOnLine){
        		new GetWSValues().execute();
        	}else{
        		new GetWSValuesOffLine().execute();
        	}
        }else{
        	values = savedInstanceState.getParcelableArrayList(KEY_STATE);
        	if(values != null){
        		init();
        	}
        }
    }
    
	@Override
	public void onResume(){
	    super.onResume();
	    this.overridePendingTransition(R.anim.fadein, R.anim.fadeout);
	}
    
    private void init(){
    	
    	adapter = new TellyAdapter(this, R.layout.list_item_telly, values);
        listSeriesTV.setAdapter(adapter);
    }
    
    private void setActionBarTitle(boolean isOnLine){
    	getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
    	if(isOnLine){
    		getActionBar().setCustomView(R.layout.action_bar_telly);
    	}else{
    		getActionBar().setCustomView(R.layout.action_bar_telly_off_line);
    	}
    }
    
    private class GetWSValues extends AsyncTask<Void, Void, Void>{

		@Override
		protected Void doInBackground(Void... arg0) {
			values = WSTelly.getInstance().getListSeriesTV(context);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			init();
		}    	
    }
    
    private class GetWSValuesOffLine extends AsyncTask<Void, Void, Void>{

		@Override
		protected void onPreExecute() {
			String text = context.getResources().getString(R.string.offline_mode);
			Toast.makeText(context, text, Toast.LENGTH_LONG).show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			TellyDB bd = new TellyDB(context);
			bd.open();			
			values = bd.getSeriesTvOffLine();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			init();
		}    	
    }    
    
    @Override
    protected void onSaveInstanceState(Bundle outState){
    	super.onSaveInstanceState(outState);
    	outState.putParcelableArrayList(KEY_STATE,(ArrayList<? extends Parcelable>) values);
    }
   
}
