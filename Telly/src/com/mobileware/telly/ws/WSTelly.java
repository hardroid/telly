package com.mobileware.telly.ws;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.mobileware.telly.R;
import com.mobileware.telly.db.TellyDB;
import com.mobileware.telly.entity.Crew;
import com.mobileware.telly.entity.Listings;
import com.mobileware.telly.entity.SeriesTV;
import com.mobileware.telly.entity.SeriesTVDetails;
import com.mobileware.telly.entity.ShowTimes;
import com.mobileware.telly.util.Utils;
import com.mobileware.telly.ws.serialize.CrewsDeserializer;
import com.mobileware.telly.ws.serialize.ListingsDeserializer;
import com.mobileware.telly.ws.serialize.SeriesTvDetailsDeserializer;
import com.mobileware.telly.ws.serialize.ShowTimesDeserializer;

import android.content.Context;
import android.util.Log;


public class WSTelly {

	private static WSTelly instance;

	public static WSTelly getInstance(){
		if(instance == null){
			instance = new WSTelly();			
		}
		return instance;
	}

	public List<SeriesTV> getListSeriesTV(Context context){
		List<SeriesTV> values = new ArrayList<SeriesTV>();
		final String WS_SERVICE = context.getResources().getString(R.string.url_showtrends);

		InputStream source = retrieveStream(WS_SERVICE);

		Gson gson = new Gson();
		Reader reader = new InputStreamReader(source);


		Type listType = new TypeToken<List<SeriesTV>>(){}.getType();
		List<SeriesTV> valuesAux = gson.fromJson(reader, listType);
		if(valuesAux != null){
			values = valuesAux;

			TellyDB bd = new TellyDB(context);
			bd.open();
			bd.insertSeriesTVValues(values);
			bd.close();
		}

		return values;
	}
	
	public SeriesTVDetails getDetailsSerieOffLine(Context contex, String jsonValue){
		SeriesTVDetails values = null;

		final GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(SeriesTVDetails.class, new SeriesTvDetailsDeserializer());
		gsonBuilder.registerTypeAdapter(Crew.class, new CrewsDeserializer());
		gsonBuilder.registerTypeAdapter(Listings.class, new ListingsDeserializer());
		gsonBuilder.registerTypeAdapter(ShowTimes.class, new ShowTimesDeserializer());

		final Gson gson = gsonBuilder.create();

		try{
			values = gson.fromJson(jsonValue, SeriesTVDetails.class);
			
		}catch(Exception ee){
			ee.printStackTrace();
			values = null;
		}

		return values;
	}

	public SeriesTVDetails getDetailsSerie(Context contex, String tvName){
		SeriesTVDetails values = null;

		final GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(SeriesTVDetails.class, new SeriesTvDetailsDeserializer());
		gsonBuilder.registerTypeAdapter(Crew.class, new CrewsDeserializer());
		gsonBuilder.registerTypeAdapter(Listings.class, new ListingsDeserializer());
		gsonBuilder.registerTypeAdapter(ShowTimes.class, new ShowTimesDeserializer());

		final Gson gson = gsonBuilder.create();

		try{

			InputStream source = retrieveStream(getPostService(contex, tvName));
			Reader reader = new InputStreamReader(source);

			values = gson.fromJson(reader, SeriesTVDetails.class);
			String jsonValue = gson.toJson(values);
			TellyDB bd = new TellyDB(contex);
			bd.open();
			bd.insertSeriesTVDetails(tvName, jsonValue);
			bd.close();
		}catch(Exception ee){
			ee.printStackTrace();
			values = null;
		}


		return values;
	}

	private String getPostService(Context context, String nameSerie){
		String wsServicePost = context.getResources().getString(R.string.url_showtrends_details);
		wsServicePost += "?url=/m/shows/";
		wsServicePost += Utils.getInstance().getWebParamFormat(nameSerie);//nameSerie.replace(" ", "-");
		return wsServicePost;
	}

	private InputStream retrieveStream(String url) {
		
		HttpParams httpParameters = new BasicHttpParams();
		HttpProtocolParams.setContentCharset(httpParameters, HTTP.UTF_8);
		HttpProtocolParams.setHttpElementCharset(httpParameters, HTTP.UTF_8);
		
		DefaultHttpClient client = new DefaultHttpClient(httpParameters);
		HttpGet getRequest = new HttpGet(url);
		try {
			HttpResponse getResponse = client.execute(getRequest);
			final int statusCode = getResponse.getStatusLine().getStatusCode();
			if (statusCode != HttpStatus.SC_OK) {
				Log.w(getClass().getSimpleName(),
						"Error " + statusCode + " for URL " + url);
				return null;
			}
			HttpEntity getResponseEntity = getResponse.getEntity();
			return getResponseEntity.getContent();
		}
		catch (IOException e) {
			getRequest.abort();
			Log.w(getClass().getSimpleName(), "Error for URL " + url, e);
		}
		return null;
	}


}
