package com.mobileware.telly.ws.serialize;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.mobileware.telly.entity.Crew;

public class CrewsDeserializer implements JsonDeserializer<Crew>, JsonSerializer<Crew>{

	@Override
	public Crew deserialize(final JsonElement json, final Type typeOfT,
			JsonDeserializationContext context) throws JsonParseException {
		
		final Crew value = new Crew();
		
		final JsonObject jsonObject = json.getAsJsonObject();
		
		final String img = jsonObject.get("img").getAsString();
		final String name = jsonObject.get("name").getAsString();
		final String charecter = jsonObject.get("char").getAsString();
		
		value.setCharecter(charecter);
		value.setImg(img);
		value.setName(name);
		
		return value;
	}

	@Override
	public JsonElement serialize(Crew values, Type type, JsonSerializationContext contextSerialition) {
		
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("img", values.getImg());
		jsonObject.addProperty("name", values.getName());
		jsonObject.addProperty("char", values.getCharecter());
		
		return jsonObject;
	}	
}
