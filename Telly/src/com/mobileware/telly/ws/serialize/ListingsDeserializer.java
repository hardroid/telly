package com.mobileware.telly.ws.serialize;

import java.lang.reflect.Type;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.mobileware.telly.entity.Listings;
import com.mobileware.telly.entity.ShowTimes;

public class ListingsDeserializer implements JsonDeserializer<Listings>, JsonSerializer<Listings>{

	@Override
	public Listings deserialize(final JsonElement json, final Type typeOfT,
			JsonDeserializationContext context) throws JsonParseException {
		
		final Listings value = new Listings();
		
		final JsonObject jsonObject = json.getAsJsonObject();
		
		final String day = jsonObject.get("day").getAsString();
		
		JsonArray jsonArray = jsonObject.getAsJsonArray("showTimes");
		JsonElement jsonElement = jsonArray.get(0);
		
		
		ShowTimes[] showTimes = context.deserialize(jsonElement, ShowTimes[].class);		
		
		value.setDay(day);
		value.setShowTimes(showTimes);
				
		return value;
	}

	@Override
	public JsonElement serialize(Listings values, Type type,JsonSerializationContext serializationContext) {
		
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("day", values.getDay());
		JsonElement showTimesElemnt = serializationContext.serialize(values.getShowTimes());
		JsonArray jsonArray = new JsonArray();
		jsonArray.add(showTimesElemnt);
		jsonObject.add("showTimes", jsonArray);
		
		return jsonObject;
	}	

}
