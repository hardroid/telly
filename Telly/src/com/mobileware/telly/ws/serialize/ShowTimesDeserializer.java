package com.mobileware.telly.ws.serialize;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.mobileware.telly.entity.ShowTimes;

public class ShowTimesDeserializer implements JsonDeserializer<ShowTimes>, JsonSerializer<ShowTimes>{

	@Override
	public ShowTimes deserialize(final JsonElement json, final Type typeOfT,
			JsonDeserializationContext context) throws JsonParseException {
		
		final ShowTimes value = new ShowTimes();
		
		final JsonObject jsonObject = json.getAsJsonObject();
		
		try{
			final String time = jsonObject.get("time").getAsString();
			final String ampm = jsonObject.get("ampm").getAsString();
			final String info = jsonObject.get("info").getAsString();
			final String air = jsonObject.get("air").getAsString();
			value.setTime(time);
			value.setAir(air);
			value.setAmpm(ampm);
			value.setInfo(info);
		}catch(Exception e){
			return null;
		}
		
		return value;
	}

	@Override
	public JsonElement serialize(ShowTimes values, Type typeOfSource, JsonSerializationContext serialitionContext) {
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("time", values.getTime());
		jsonObject.addProperty("ampm", values.getAmpm());
		jsonObject.addProperty("info", values.getInfo());
		jsonObject.addProperty("air", values.getAir());
		
		return jsonObject;
	}
}
