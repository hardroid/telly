package com.mobileware.telly.ws.serialize;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.mobileware.telly.entity.Crew;
import com.mobileware.telly.entity.Listings;
import com.mobileware.telly.entity.SeriesTVDetails;

public class SeriesTvDetailsDeserializer implements JsonDeserializer<SeriesTVDetails>, JsonSerializer<SeriesTVDetails>{

	@Override
	public SeriesTVDetails deserialize(final JsonElement json, final Type typeOfT,
			JsonDeserializationContext context) throws JsonParseException {
		
		final SeriesTVDetails value = new SeriesTVDetails();
		
		final JsonObject jsonObject = json.getAsJsonObject();
		
		final String name = jsonObject.get("name").getAsString();
		final String air = jsonObject.get("air").getAsString();
		final String img = jsonObject.get("img").getAsString();
		
		Crew[] crews = context.deserialize(jsonObject.get("crew"), Crew[].class);
		
		final String info = jsonObject.get("info").getAsString();
		
		Listings[] listings = context.deserialize(jsonObject.get("listings"), Listings[].class);
		
		value.setAir(air);
		value.setCrews(crews);
		value.setImg(img);
		value.setInfo(info);
		value.setListings(listings);
		value.setName(name);
		
		return value;
	}

	@Override
	public JsonElement serialize(SeriesTVDetails values, Type type,
			JsonSerializationContext serializationContext) {
		
		JsonObject jsonObject=new JsonObject();
		jsonObject.addProperty("name", values.getName());
		jsonObject.addProperty("air", values.getAir());
		jsonObject.addProperty("img", values.getImg());
		JsonElement jsonElementsCrew = serializationContext.serialize(values.getCrews());
		jsonObject.add("crew", jsonElementsCrew);
		jsonObject.addProperty("info", values.getInfo());
		JsonElement jsonElemntsListings = serializationContext.serialize(values.getListings());
		jsonObject.add("listings", jsonElemntsListings);
		return jsonObject;
	}
	
	

	
	

}
