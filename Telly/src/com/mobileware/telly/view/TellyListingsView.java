package com.mobileware.telly.view;

import com.mobileware.telly.R;
import com.mobileware.telly.entity.ShowTimes;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

public class TellyListingsView extends LinearLayout{
	
	private TellyTextView txtListingsDay;
	private TellyTextView txtListingTime;
	private TellyTextView txtListingAM_PM;
	private TellyTextView txtListingInfo;
	
	
	public TellyListingsView(Context context){
		super(context);
	}

	public TellyListingsView(Context context, AttributeSet attrs, ShowTimes values, String day, boolean isShowDay) {
		super(context, attrs);
		
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		LinearLayout customView = (LinearLayout) inflater.inflate(R.layout.listing_item_telly, this);
		
		if(isShowDay){
			txtListingsDay = (TellyTextView) customView.findViewById(R.id.txtListingsDay);
			txtListingsDay.setVisibility(View.VISIBLE);
			txtListingsDay.setText(day);
		}
		
		txtListingTime = (TellyTextView) customView.findViewById(R.id.txtListingTime);
		txtListingTime.setText(values.getTime());
		
		txtListingAM_PM = (TellyTextView) customView.findViewById(R.id.txtListingAM_PM);
		txtListingAM_PM.setText(values.getAmpm());
		
		txtListingInfo = (TellyTextView) customView.findViewById(R.id.txtListingInfo);
		txtListingInfo.setText(values.getInfo());
		
	}
	
	
	
	

}
