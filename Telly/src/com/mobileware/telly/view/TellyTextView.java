package com.mobileware.telly.view;

import com.mobileware.telly.util.Utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class TellyTextView extends TextView{
	
	public TellyTextView(Context context) {
		super(context);
		setFont();
	}

	public TellyTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setFont();
	}

	public TellyTextView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setFont();
	}	
	
	private void setFont(){
		Utils.getInstance().setTellyTextFont(getContext(), this);
	}

}
