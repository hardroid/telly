package com.mobileware.telly;

import com.mobileware.telly.db.TellyDB;
import com.mobileware.telly.entity.Listings;
import com.mobileware.telly.entity.SeriesTVDetails;
import com.mobileware.telly.entity.ShowTimes;
import com.mobileware.telly.util.Utils;
import com.mobileware.telly.view.TellyListingsView;
import com.mobileware.telly.view.TellyTextView;
import com.mobileware.telly.ws.WSTelly;
import com.squareup.picasso.Picasso;

import android.support.v4.app.FragmentActivity;
import android.app.ActionBar;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

public class ShowDetailsActivity extends FragmentActivity {

	private Context context;
	
	private SeriesTVDetails values;
	private String tvName;

	private ImageView imgFront;
	private TellyTextView txtTitleSerie;
	private TellyTextView txtAirSerie;
	private TellyTextView txtDescriptionSerie;

	private ScrollView scrollViewContent;
	private RelativeLayout layoutLoading;
	private LinearLayout linearListingInfo;
		
	private final String KEY_STATE = "outState";
	private final String KEY_NAME = "outName";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_details);
		context = this;
		boolean isOnLine = Utils.isOnlinee(context);
		setActionBarTitle(isOnLine);
		layoutLoading = (RelativeLayout)findViewById(R.id.layoutLoading);
		scrollViewContent = (ScrollView)findViewById(R.id.scrollViewContent);
		if(savedInstanceState == null){
			if(isOnLine){
				tvName = getIntent().getStringExtra(Utils.PARAM_NAME);
				new GetWSValues().execute();
			}else{
				tvName = getIntent().getStringExtra(Utils.PARAM_NAME);
				new GetValuesOffLine().execute();
			}
			
		}else{
			values = savedInstanceState.getParcelable(KEY_STATE);
			tvName = savedInstanceState.getParcelable(KEY_NAME);
        	if(values != null){
        		init();
        	}
		}
	}

	@Override
	public void onResume(){
		super.onResume();
		this.overridePendingTransition(R.anim.fadein, R.anim.fadeout);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState){
		super.onSaveInstanceState(outState);
		outState.putParcelable(KEY_STATE, values);
		outState.putString(KEY_NAME, tvName);
	}

	private void init(){
		if(values != null){
			imgFront = (ImageView)findViewById(R.id.imgFront);
			Picasso.with(this).load(values.getImg())
			.placeholder(R.drawable.ic_launcher)
			.fit()
			.centerCrop()
			.into(imgFront);

			//Set text Info Values
			txtTitleSerie = (TellyTextView) findViewById(R.id.txtTitleSerie);
			txtAirSerie = (TellyTextView) findViewById(R.id.txtAirSerie);
			txtDescriptionSerie = (TellyTextView) findViewById(R.id.txtDescriptionSerie);

			txtTitleSerie.setText(values.getName());
			txtAirSerie.setText(values.getAir());
			txtDescriptionSerie.setText(values.getInfo());

			if(values.getListings() != null){

				
				layoutLoading.setVisibility(View.GONE);
				scrollViewContent.setVisibility(View.VISIBLE);
				linearListingInfo = (LinearLayout) scrollViewContent.findViewById(R.id.linearListingInfo);

				for(int a = 0 ; a < values.getListings().length ; a++){
					Listings aux = values.getListings()[a];
					boolean isShowDay = true;
					if(aux != null){
						ShowTimes[] showArrayAux = aux.getShowTimes();
						if(showArrayAux != null){
							for(int b = 0 ; b < showArrayAux.length; b++){
								ShowTimes showAux = showArrayAux[b];
								TellyListingsView viewCustom = new TellyListingsView(this, null, showAux, aux.getDay(), isShowDay);
								isShowDay = false;
								linearListingInfo.addView(viewCustom);
							}
						}

					}
				}
			}

		}
	}

	private void setActionBarTitle(boolean isOnLine){
		getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM); 
		if(isOnLine){
			getActionBar().setCustomView(R.layout.action_bar_telly);
		}else{
			getActionBar().setCustomView(R.layout.action_bar_telly_off_line);
		}
		TellyTextView txtTitle = (TellyTextView)getActionBar().getCustomView().findViewById(R.id.tellyTextView1);
		txtTitle.setText(getResources().getString(R.string.title_activity_show_details));
	}
	
	private void notFoundData(){
		finish();
	}

	private class GetWSValues extends AsyncTask<Void, Void, SeriesTVDetails>{

		@Override
		protected SeriesTVDetails doInBackground(Void... arg0) {
			values = null;
			values = WSTelly.getInstance().getDetailsSerie(context, tvName);
			return values;
		}

		@Override
		protected void onPostExecute(SeriesTVDetails result) {
			if(result != null){
				init();
			}else{
				String text = context.getResources().getString(R.string.error_json_read);
				Toast.makeText(context, text, Toast.LENGTH_LONG).show();
				notFoundData();
			}
			
		}    	
	}
	
	private class GetValuesOffLine extends AsyncTask<Void, Void, SeriesTVDetails>{

		@Override
		protected SeriesTVDetails doInBackground(Void... arg0) {
			values = null;
			TellyDB bd = new TellyDB(context);
			bd.open();
			
			String jsonValue = bd.getJsonValueOffLine(tvName);
			if(jsonValue.length() > 0){
				values = WSTelly.getInstance().getDetailsSerieOffLine(context, jsonValue);
			}
			return values;
		}

		@Override
		protected void onPostExecute(SeriesTVDetails result) {
			if(result != null){
				init();
			}else{
				String text = context.getResources().getString(R.string.offline_mode_not_found);
				Toast.makeText(context, text, Toast.LENGTH_LONG).show();
				notFoundData();
			}
			
		}    	
	}
}
