package com.mobileware.telly.adapter;

import java.util.List;

import com.mobileware.telly.R;
import com.mobileware.telly.ShowDetailsActivity;
import com.mobileware.telly.entity.SeriesTV;
import com.mobileware.telly.util.Utils;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class TellyAdapter extends ArrayAdapter<SeriesTV>{
	
	private final LayoutInflater mLayoutInflater;
	private Context context;	

	public TellyAdapter(Context context, int resource, List<SeriesTV> objects) {
		super(context, resource, objects);
		this.context = context;
		this.mLayoutInflater = LayoutInflater.from(context);
	}
	

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder vh;

		final SeriesTV serie = getItem(position);

		if(convertView == null){
			convertView = mLayoutInflater.inflate(R.layout.list_item_telly, parent, false);
			vh = new ViewHolder();
			vh.imgPreview = (ImageView)convertView.findViewById(R.id.imgPreview);
			vh.txtTitle = (TextView)convertView.findViewById(R.id.txtTitle);
			vh.txtDescription = (TextView)convertView.findViewById(R.id.txtDescription);
			Utils.getInstance().setTellyTextFont(getContext(), vh.txtTitle);
			Utils.getInstance().setTellyTextFont(getContext(), vh.txtDescription);	
			convertView.setTag(vh);
		} else{
			vh = (ViewHolder)convertView.getTag();	
		}
		
		Picasso.with(context).load(serie.getImg()).placeholder(R.drawable.ic_launcher).into(vh.imgPreview);
		
		vh.txtDescription.setText(getContext().getResources().getString(R.string.air_text) + " " + serie.getAir());
		vh.txtTitle.setText(serie.getName());
		
		
		convertView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getContext(), ShowDetailsActivity.class);
				i.putExtra(Utils.PARAM_NAME, serie.getName());
				i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				getContext().startActivity(i);
			}
		});
		
		return convertView;
	}

	
	static class ViewHolder{
		ImageView imgPreview;
		TextView txtTitle;
		TextView txtDescription;
	}
}
