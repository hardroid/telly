package com.mobileware.telly.db;

import java.util.ArrayList;
import java.util.List;

import com.mobileware.telly.entity.SeriesTV;
import com.mobileware.telly.util.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class TellyDB {
	
	private static final String NOMBRE_BD = "pg_participantes.db";
	private static final String APP_NAME = "Gillette Barca";
	private static final int VERSION = 2;
	
	private static final String TB_SERIES_TV= "SERIES_TV";
	private static final String TB_SERIES_TV_DET= "SERIES_TV_DET";
	
	private static final String NUM = "NUM";
	private static final String LNK = "LNK";
	private static final String NAME = "NAME";
	private static final String AIR = "AIR";
	private static final String IMG = "IMG";
	
	private static final String NAME_DET = "NAME_DET";
	private static final String VALUE_DET_JSON = "VALUE_DET_JSON";	
	
	
	private static final String TABLA_SERIES_TV = 
			"CREATE TABLE "+TB_SERIES_TV+" (" +
			" _id INTEGER PRIMARY KEY AUTOINCREMENT," +
			" "+NUM+" INTEGER not null," +
			" "+IMG+" text not null," +
			" "+LNK+" text not null," +
			" "+NAME+" text not null," +
			" "+AIR+" text not null);";
	
	private static final String TABLA_SERIES_TV_DET = 
			"CREATE TABLE "+TB_SERIES_TV_DET+" (" +
			" _id INTEGER PRIMARY KEY AUTOINCREMENT," +
			" "+NAME_DET+" text not null," +		
			" "+VALUE_DET_JSON+" text not null );";
	
	final Context context;
	DataBaseHelper DBHelper;
	SQLiteDatabase db;
	
	public TellyDB(Context context) 
	{
		this.context = context;
		DBHelper = new DataBaseHelper(context);
	}
	
	
	private static class DataBaseHelper extends SQLiteOpenHelper
	{
		public DataBaseHelper(Context ctx) {
			// TODO Auto-generated constructor stub
			super(ctx, NOMBRE_BD, null, VERSION);
		}
		
		@Override
		public void onCreate(SQLiteDatabase db)
		{
			try{
				db.execSQL(TABLA_SERIES_TV);
				db.execSQL(TABLA_SERIES_TV_DET);
			}catch(SQLException ex)
			{
				ex.printStackTrace();
			}
		}
		
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
		{
			Log.w(APP_NAME, "Actualizando BD de version " + oldVersion + 
					" a nueva version " + newVersion + " Todos los datos se eliminaran" );
			
			db.execSQL("DROP TABLE IF EXISTS "+TABLA_SERIES_TV);
			db.execSQL("DROP TABLE IF EXISTS "+TABLA_SERIES_TV_DET);
			onCreate(db);			
		}
		
	}
	
	public TellyDB open()
	{
		db = DBHelper.getWritableDatabase();
		return this;
	}
	
	public void close()
	{
		DBHelper.close();
	}
	
	
	public long insertSeriesTVValues(List<SeriesTV> values){
		long rowCount = 0;
		deleteRowsSeriesTV();
		for(SeriesTV aux : values){
			rowCount = insertSeriesTV(aux);
		}
		return rowCount;
	}
	
	
	private long insertSeriesTV(SeriesTV p) 
	{	
		ContentValues valoresIniciales = new ContentValues();		
		valoresIniciales.put(NUM, p.getNum());
		valoresIniciales.put(LNK, p.getLnk());
		valoresIniciales.put(NAME, p.getName());
		valoresIniciales.put(AIR, p.getAir());
		valoresIniciales.put(IMG, p.getImg());
				
		return db.insert(TB_SERIES_TV, null, valoresIniciales);
	}
	
	private void deleteRowsSeriesTV(){
		db.delete(TB_SERIES_TV, null, null);
	}
	
	//CRUD SERIES TV DET
	
	private void deleteSeriesTVDetails(String name){
		String where = NAME_DET + " = '"+name+"'";
		
		db.delete(TB_SERIES_TV_DET, where, null);
	}
	
	public long insertSeriesTVDetails(String name, String jsonValue){
		String valueName = Utils.getInstance().getWebParamFormat(name);
		deleteSeriesTVDetails(valueName);
		
		ContentValues valoresIniciales = new ContentValues();		
		valoresIniciales.put(NAME_DET, valueName);
		valoresIniciales.put(VALUE_DET_JSON, jsonValue);
		return db.insert(TB_SERIES_TV_DET, null, valoresIniciales);
	}
	
	public List<SeriesTV> getSeriesTvOffLine(){
		List<SeriesTV> values = new ArrayList<SeriesTV>();
		Cursor c;
		String [] columns = {NUM, LNK, NAME, AIR, IMG};
		c = db.query(TB_SERIES_TV, columns, null, null, null, null, null);
		
		if(c.moveToFirst()){
		do{
			SeriesTV aux = new SeriesTV();
			aux.setNum(c.getInt(0));
			aux.setLnk(c.getString(1));
			aux.setName(c.getString(2));
			aux.setAir(c.getString(3));
			aux.setImg(c.getString(4));
			values.add(aux);
			
		}while(c.moveToNext());		
		}
		return values;
	
	}
	
	public String getJsonValueOffLine(String tvName){
		String jsonValue = "";
		String [] columns = {VALUE_DET_JSON};
		String where = NAME_DET + " = '" + Utils.getInstance().getWebParamFormat(tvName) + "' ";
		Cursor c;
		c = db.query(TB_SERIES_TV_DET, columns, where, null, null, null, null);
		if(c.moveToFirst()){
			jsonValue = c.getString(0);
		}
		return jsonValue;
	}

}
