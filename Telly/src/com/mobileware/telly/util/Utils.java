package com.mobileware.telly.util;

import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.TextView;

public class Utils {

	private static Utils instance;
	
	public final static String PARAM_NAME = "name"; 

	public static Utils getInstance(){
		if(instance == null){
			instance = new Utils();
		}

		return instance;
	}

	public void setTellyTextFont(Context context, TextView v){		
		Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/BadScript.ttf");
		v.setTypeface(font);			
	}	
	
	public String getWebParamFormat(String value){
		String valueFormat = value.replace(" ", "-");
		valueFormat = valueFormat.replace("'", "");
		valueFormat = valueFormat.replace(".", "");
		
		return valueFormat;
	}
	
	public static boolean isOnlinee(Context context) {
		try{
			ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo netInfo = cm.getActiveNetworkInfo();
			if (netInfo != null && netInfo.isConnectedOrConnecting()) {
				return true;
			}
		}catch(Exception eee){
			eee.printStackTrace();
		}
		
		return false;
	}
	
}
